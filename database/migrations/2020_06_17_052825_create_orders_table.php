<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('status',['PLACED', 'ACCEPTED', 'PICKUP', 'DELIVERED', 'CANCELLED']);
            $table->unsignedBigInteger('delivery_person_id')->nullable();
            $table->bigInteger('est_delivery_time');
            $table->string('request_input');
            $table->timestamps();
            $table->foreign('delivery_person_id')->references('id')->on('delivery_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
