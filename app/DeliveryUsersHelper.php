<?php


namespace App;


class DeliveryUsersHelper
{

    public static function listActiveDeliveryUsers()
    {
        $deliveryUsers = DeliveryUsers::select('status', 'name', 'est_delivery_time')
            ->join('orders', 'orders.id', '=', 'curr_order_id')
            ->get()
            ->toArray();

        foreach ($deliveryUsers as $idx => $deliveryUser) {
            $time_left = $deliveryUser['est_delivery_time'] - microtime(true);
            $time_status = $time_left > 0 ? ' LEFT' : ' DELAY';
            $deliveryUsers[$idx]['est_delivery_time'] = date('r',$deliveryUser['est_delivery_time']+19800);
            $deliveryUsers[$idx]['time_left'] = round(ceil(abs($time_left / 60)), 1) . ' MINUTES' . $time_status;
        }
        return $deliveryUsers;
    }

    public static function setDeliveryPartnerInActive($delivery_person_id)
    {
        $deliveryUser = DeliveryUsers::find($delivery_person_id);
        $deliveryUser->curr_order_id = 0;
        return $deliveryUser->save();
    }
}
