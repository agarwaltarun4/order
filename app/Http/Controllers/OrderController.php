<?php


namespace App\Http\Controllers;





use App\DeliveryUsersHelper;
use App\OrderHelper;
use App\OrderStatus;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
    }

    public function create(Request $request){
        $orderInfo = $request->all();
        $orderId = OrderHelper::createOrder($orderInfo);
        $response = [];
        if($orderId!=0){
            $response['status'] = OrderStatus::PLACED;
            $response['orderId'] = $orderId;
        }else{
            $response['status'] = 'ERROR';
        }
        JsonResponse::create($response)->send();
    }

    public function getStatus(Request $request){
        $orderId = $request->get('orderId');
        $status = OrderHelper::getStatus($orderId);
        $response['status'] = "No order found";
        if(!empty($status)){
            $response['status'] = $status;
        }
        JsonResponse::create($response)->send();
    }

    public function updateStatus(Request $request){
        $orderId = $request->get('orderId');
        $status = $request->get('status');

        $response['status'] = 'FAILURE';
        if(OrderHelper::updateStatus($orderId,$status)){
            $response['status'] = 'SUCCESS';
        }
        return JsonResponse::create($response)->send();
    }

    public function listDeliveryUsers(){
        return JsonResponse::create(DeliveryUsersHelper::listActiveDeliveryUsers())->send();
    }
}
