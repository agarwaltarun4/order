<?php


namespace App;


class OrderStatus
{
    const PLACED = 'PLACED';
    const PICKUP = 'PICKUP';
    const DELIVERED = 'DELIVERED';
    const CANCELLED = 'CANCELLED';
}
