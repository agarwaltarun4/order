<?php


namespace App;


class OrderHelper
{

    public static function createOrder($orderInfo){
        $order = new Orders();
        $order->status = OrderStatus::PLACED;
        $order->est_delivery_time = microtime(true)+30*60;
        $order->request_input = json_encode($orderInfo);
        if($order->save()){
            return $order->id;
        }
        return 0;
    }

    public static function getStatus($orderId)
    {
        $order = Orders::select('status')
            ->where('id','=',$orderId)
            ->get()
            ->toArray();
        if(!empty($order)) {
            return $order[0]['status'];
        }
        return "";
    }

    public static function updateStatus($orderId, $status)
    {
        $order = Orders::find($orderId);
        if(empty($order))return false;
        $order->status = $status;
        $order->save();
        if($status == OrderStatus::DELIVERED) {
            return DeliveryUsersHelper::setDeliveryPartnerInActive($order->delivery_person_id);
        }
        return true;
    }


}
