<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/order/create','OrderController@create');
Route::get('/order/status','OrderController@getStatus');
Route::post('/order/status','OrderController@updateStatus');
Route::get('/listActiveUsers','OrderController@listDeliveryUsers');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
